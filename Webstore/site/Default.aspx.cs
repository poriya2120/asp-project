﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class code_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\1.aspx");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\2.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\3.aspx");
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\4.aspx");
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\6.aspx");
    }

    protected void Button6_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\7.aspx");
    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\8.aspx");
    }

    protected void Button8_Click(object sender, EventArgs e)
    {
        Response.Redirect(@"~\site\10.aspx");
    }
}