﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default4.aspx.cs" Inherits="Default4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 35px;
        }
        .lables {

            text-align:center;
        }
             .lable{

            text-align:right;
        }
        .div {

            margin:auto;
            background-color:dimgray;
        }
        .li {
             background-color:dimgray;
            margin-bottom: 0px;
            margin:0 auto;
          
        }
        .s1 {
            float:right;
        }


    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div  class="div" style="height: 1215px">

    
        <asp:DataList CssClass="li" ID="DataList1" runat="server" DataKeyField="proid" DataSourceID="SqlDataSource1" OnSelectedIndexChanged="DataList1_SelectedIndexChanged" RepeatColumns="5" RepeatDirection="Horizontal" BackColor="dimgray" Width="1365px" OnItemCommand="DataList1_ItemCommand">
            <EditItemStyle CssClass="s1" />
            <ItemTemplate>
                <table  class="auto-style1">
                    <tr>
                        <td>
                            <asp:Label  CssClass="lables" ID="Label3" runat="server" BackColor="Red" BorderColor="Black" Font-Bold="True" Font-Size="Large" Text='<%# Eval("proid") %>' Width="323px" BorderStyle="Solid" BorderWidth="3px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <asp:Image ID="Image2" runat="server" Height="232px" ImageUrl= '<%# Eval("proimage") %>'  Width="321px" BorderColor="Black" BorderStyle="Solid" BorderWidth="3px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2">
                            <asp:Label CssClass="lables" ID="Label1" runat="server" BackColor="#009900" Height="59px" Text='<%# Eval("protitle") %>' Width="323px" BorderColor="Black" BorderStyle="Solid" BorderWidth="3px"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Label CssClass="lables" ID="Label2" runat="server" BackColor="#0099CC" Height="102px" Text='<%# Eval("prodic") %>' Width="323px" BorderColor="Black" BorderStyle="Solid" BorderWidth="3px"></asp:Label>
                <br />
                
                <asp:Button ID="Button1" runat="server" Height="32px" OnClick="Button1_Click" Text="Button" Width="324px" BorderColor="Black" BorderStyle="Solid" BorderWidth="3px" CommandArgument='<%# Eval("proId")%>' CommandName="showdetail" />
                <br />
                <br />
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:spicestoreConnectionString %>" SelectCommand="SELECT * FROM [product3]" ProviderName="System.Data.SqlClient"></asp:SqlDataSource>
    
        <br />
    
    </div>
    </form>
</body>
</html>
